
def execute() {
    def host = deployConfig['redis']['host']
    def port = deployConfig['redis']['port']
    def dbnum = deployConfig['redis']['dbnum']
    def poolSize = deployConfig['redis']['poolSize']
    def enable = deployConfig['redis']["enable"]
    if (host) {
        if (enable) {
            def variable = "//<enable.redis>"
            def value = enable.replace("#host#", host)
            value = value.replace("#port#", port.toString())
            value = value.replace("#dbnum#", dbnum.toString())
            value = value.replace("#poolSize#", poolSize.toString())
            println("Binding variable " + variable + " with " + value)
            sh """
                ${env.WORKSPACE}/scripts/replace.sh "${env.WORKSPACE}/${env.tempModulePath}" "${variable}" "${value}"
            """                              
        } 
    } else {
        def variable = "//<enable.redis>"
        def value = ""
        sh """
            ${env.WORKSPACE}/scripts/replace.sh "${env.WORKSPACE}/${env.tempModulePath}" "${variable}" "${value}"
        """ 
    }
}

return this
