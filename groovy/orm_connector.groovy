
def execute(dbConnectors) {
    def connectorSize = dbConnectors.size()
    def i = 0
    println(connectorSize)
    for(connector in dbConnectors) {
        println(i)
        def name = connector.name
        def db_type = connector.db_type
        def driver_name = connector.driver_name
        def connstring = connector.connstring
        def timeout = connector.timeout
        def max_idleconn = connector.max_idleconn
        def max_openconn = connector.max_openconn
        def enable = connector.enable
        if (name) {
            if (enable) {
                def variable = "//<enable.db_connectors>"
                def value = enable.replace("#connstring#", connstring)
                value = value.replace("#name#", name)
                value = value.replace("#driver_name#", driver_name)
                value = value.replace("#db_type#", db_type)
                value = value.replace("#max_idleconn#", max_idleconn.toString())
                value = value.replace("#max_openconn#", max_openconn.toString())
                
                if (i < (connectorSize - 1)) {
                    value = value + "<next_line>//<enable.db_connectors>"
                }
                i = i + 1
                println("Binding variable " + variable + " with " + value)
                sh """
                    ${env.WORKSPACE}/scripts/replace.sh "${env.WORKSPACE}/${env.tempModulePath}" "${variable}" "${value}"
                """                              
            } 
        }
    }
}

return this