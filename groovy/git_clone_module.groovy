
def execute(moduleArray) {
    for(module in moduleArray) {
        def moduleRepository = module.repository
        def modulePath = module.path
        def modulePackage = module.package
        def moduleVersion = module.version
        println("Clone module source for " + moduleRepository)
        sh "git clone ssh://git@bitbucket.beebuddy.net:7999/${moduleRepository} ${env.WORKSPACE}/${modulePath}"
        println("Merge module source for " + moduleRepository)
        sh "${env.WORKSPACE}/scripts/merge.sh ${env.WORKSPACE}/${modulePath} ${env.WORKSPACE}/${env.tempModulePath}"
        println("Replace module package for " + moduleRepository)
        sh """
            ${env.WORKSPACE}/scripts/replace.sh "${env.WORKSPACE}/${tempModulePath}" "${modulePackage}" "${env.mainPackage}"
        """
        println("Replace module template for " + moduleRepository)
        sh """
            ${env.WORKSPACE}/scripts/replace.sh "${env.WORKSPACE}/${tempModulePath}" "patanayont-go/msa-template" "${env.mainPackage}"
        """
    }
}

return this