
def execute() {
    def filename = deployConfig['file_logger']['filename']
    def maxlines = deployConfig['file_logger']['maxlines']
    def maxsize = deployConfig['file_logger']['maxsize']
    def daily = deployConfig['file_logger']['daily']
    def maxdays = deployConfig['file_logger']['maxdays']
    def rotate = deployConfig['file_logger']['rotate']
    def level = deployConfig['file_logger']['level']
    def init = deployConfig['file_logger']['init']

    if (filename) {
        if (init) {
            def variable = "//<init.file_logger>"
            def value = init.replace("#filename#", filename)
            value = value.replace("#maxlines#", maxlines.toString())
            value = value.replace("#maxsize#", maxsize.toString())
            value = value.replace("#daily#", daily.toString())
            value = value.replace("#maxdays#", maxdays.toString())
            value = value.replace("#rotate#", rotate.toString())
            value = value.replace("#level#", level.toString())

            println("Binding variable " + variable + " with " + value)
            sh """
                ${env.WORKSPACE}/scripts/replace.sh "${env.WORKSPACE}/${env.tempModulePath}" "${variable}" "${value}"
            """                              
        } 
    }
}

return this