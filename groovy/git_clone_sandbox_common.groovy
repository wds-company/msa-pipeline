
def execute() {
    sh "git clone ssh://git@bitbucket.partsoto.net:7799/${env.sandboxCommonRepo} ${env.WORKSPACE}/${env.sandboxCommonPath}"
    sh "${env.WORKSPACE}/scripts/merge_sandbox.sh ${env.WORKSPACE}/${env.sandboxCommonPath} ${env.WORKSPACE}/${env.tempModulePath}"
    sh """
            ${env.WORKSPACE}/scripts/delete_line.sh "${env.WORKSPACE}/${tempModulePath}/controllers"
    """
    sh """
            ${env.WORKSPACE}/scripts/delete_line.sh "${env.WORKSPACE}/${tempModulePath}/accessors"
    """
    sh """
            ${env.WORKSPACE}/scripts/replace.sh "${env.WORKSPACE}/${tempModulePath}" "xFunction." "c."
    """
}

return this