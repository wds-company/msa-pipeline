
def execute(configFile) {
    def exists = fileExists "deploy_config/${configFile}.json"
    if (exists) {
        echo "deploy_config/${configFile}.json found"
    } else {
        error("deploy_config/${configFile}.json cannot be found")
    }
    def files = null
    try {
        deployConfig = readJSON file: "deploy_config/${configFile}.json"
    } catch (Exception e) {
        error("Cannot read deploy_config/${configFile}.json file.\nError:\n${e}")
    }
    try {
        env.mainPackage = deployConfig['main_package']
        env.moduleDirectory = deployConfig['module_directory']
        env.tempModulePath = deployConfig['temp_module_path']
        env.appUser = deployConfig['app_user']
        env.appKey = deployConfig['app_key']
        env.templateRepo = deployConfig['template']['repository']
        env.templateVersion = deployConfig['template']['version']
        env.commonPath = deployConfig['common']['path']
        env.commonRepo = deployConfig['common']['repository']
        env.containerName = deployConfig['container']['name']
        env.imageName = deployConfig['container']['image']
        env.dockerInputFile = deployConfig['container']['docker_file']
        env.dockerComposeFile = deployConfig['container']['compose_file']
        env.sandboxCommonPath = deployConfig['sandbox_common']['path']
        env.sandboxCommonRepo = deployConfig['sandbox_common']['repository']
        return deployConfig
    } catch (Exception e) {
        error("Cannot read deploy_config/${configFile}.json property: deployment.files\nError:\n${e}")
    }
}

return this