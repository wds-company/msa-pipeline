
def execute() {
    def host = deployConfig['elastic_logger']['host']
    def port = deployConfig['elastic_logger']['port']
    def init = deployConfig['elastic_logger']["init"]
    def binding_var = deployConfig['elastic_logger']['binding_var']
    if (host) {
        if (init) {
            def variable = "//<init.elastic_logger>"
            def value = init.replace("#host#", host)
            value = value.replace("#port#", port.toString())
            println("Binding variable " + variable + " with " + value)
            sh """
                ${env.WORKSPACE}/scripts/replace.sh "${env.WORKSPACE}/${env.tempModulePath}" "${variable}" "${value}"
            """                              
        }  
        println("Binding variable " + binding_var)
        if (binding_var) {
            def variable = "//<elastic_logger.host>"
            def value = 'elastic_host = "' + host + '"'
            sh """
                ${env.WORKSPACE}/scripts/replace.sh "${env.WORKSPACE}/${env.tempModulePath}" "${variable}" "${value}"
            """
            variable = "//<elastic_logger.port>"
            value = 'elastic_port = ' + port
            sh """
                ${env.WORKSPACE}/scripts/replace.sh "${env.WORKSPACE}/${env.tempModulePath}" "${variable}" "${value}"
            """
        }
    } else {
        def variable = "//<init.elastic_logger>"
        def value = ""
        sh """
            ${env.WORKSPACE}/scripts/replace.sh "${env.WORKSPACE}/${env.tempModulePath}" "${variable}" "${value}"
        """ 
    }
}

return this