
def execute() {
    def name = deployConfig['rpc_connector']['name']
    def host = deployConfig['rpc_connector']['host']
    def port = deployConfig['rpc_connector']['port']
    def timeout = deployConfig['rpc_connector']['timeout']
    def enable = deployConfig['rpc_connector']["enable"]
    if (name) {
        if (enable) {
            def variable = "//<enable.rpc_connector>"
            def value = enable.replace("#host#", host)
            value = value.replace("#port#", port.toString())
            value = value.replace("#timeout#", timeout.toString())
            value = value.replace("#name#", name)
            println("Binding variable " + variable + " with " + value)
            sh """
                ${env.WORKSPACE}/scripts/replace.sh "${env.WORKSPACE}/${env.tempModulePath}" "${variable}" "${value}"
            """                              
        } 
    } else {
        def variable = "//<enable.rpc_connector>"
        def value = ""
        sh """
            ${env.WORKSPACE}/scripts/replace.sh "${env.WORKSPACE}/${env.tempModulePath}" "${variable}" "${value}"
        """ 
    }
}

return this