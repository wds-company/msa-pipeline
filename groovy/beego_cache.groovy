
def execute() {
    def host = deployConfig['beegocache']['host']
    def port = deployConfig['beegocache']['port']
    def dbnum = deployConfig['beegocache']['dbnum']
    def key = deployConfig['beegocache']['key']
    def init = deployConfig['beegocache']["init"]
    def enable = deployConfig['beegocache']["enable"]
    if (host) {
        if (init) {
            def variable = "//<init.beegocache>"
            def value = init.replace("#key#", key)
            value = value.replace("#host#", host)
            value = value.replace("#port#", port.toString())
            value = value.replace("#dbnum#", dbnum.toString())
            println("Binding variable " + variable + " with " + value)
            sh """
                ${env.WORKSPACE}/scripts/replace.sh "${env.WORKSPACE}/${env.tempModulePath}" "${variable}" "${value}"
            """                              
        }
        if (enable) {
            def variable = "//<enable.beegocache>"
            def value = enable
            println("Binding variable " + variable + " with " + value)
            sh """
                ${env.WORKSPACE}/scripts/replace.sh "${env.WORKSPACE}/${env.tempModulePath}" "${variable}" "${value}"
            """                              
        } 
    }
}

return this