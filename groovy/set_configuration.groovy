
def execute() {
    env.getDataOnly = deployConfig['getDataOnly']
    env.checkDataSection = deployConfig['checkDataSection']
    env.tempUploadDir = deployConfig['tempUploadDir']
    if (env.getDataOnly.toBoolean()) {
        sh """
            ${env.WORKSPACE}/scripts/replace.sh "${env.WORKSPACE}/${env.tempModulePath}" "//<getDataOnly>" "getDataOnly = true"
        """
    } else {
        sh """
            ${env.WORKSPACE}/scripts/replace.sh "${env.WORKSPACE}/${env.tempModulePath}" "//<getDataOnly>" "getDataOnly = false"
        """
    }
    if (env.checkDataSection.toBoolean()) {
        sh """
            ${env.WORKSPACE}/scripts/replace.sh "${env.WORKSPACE}/${env.tempModulePath}" "//<checkDataSection>" "checkDataSection = true"
        """
    } else {
        sh """
            ${env.WORKSPACE}/scripts/replace.sh "${env.WORKSPACE}/${env.tempModulePath}" "//<checkDataSection>" "checkDataSection = false"
        """
    }
    if (env.tempUploadDir.toBoolean()) {
        sh """
            ${env.WORKSPACE}/scripts/replace.sh "${env.WORKSPACE}/${env.tempModulePath}" "//<tempUploadDir>" 'tempUploadDir = "/shared/docs/"'
        """
    } else {
        sh """
            ${env.WORKSPACE}/scripts/replace.sh "${env.WORKSPACE}/${env.tempModulePath}" "//<tempUploadDir>" 'tempUploadDir = "/tmp"'
        """
    }
}

return this