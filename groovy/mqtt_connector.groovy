
def execute(mqttConnectors) {
    def connectorSize = mqttConnectors.size()
    def i = 0
    println(connectorSize)
    for(connector in mqttConnectors) {
        println(i)
        def name = connector.name
        def url = connector.url
        def username = connector.username
        def password = connector.password
        def enable = connector.enable
        def enable_subscriber = connector.enable_subscriber
        if (name) {
            if (enable) {
                def variable = "//<enable.mqtt_connectors>"
                def value = enable.replace("#url#", url)
                value = value.replace("#name#", name)
                value = value.replace("#username#", username)
                value = value.replace("#password#", password)
                
                if (i < (connectorSize - 1)) {
                    value = value + "<next_line>//<enable.mqtt_connectors>"
                }
                i = i + 1
                println("Binding variable " + variable + " with " + value)
                sh """
                    ${env.WORKSPACE}/scripts/replace.sh "${env.WORKSPACE}/${env.tempModulePath}" "${variable}" "${value}"
                """                              
            } 
            if (enable_subscriber) {
                def variable = "//<enable_subscriber>"
                def value = enable_subscriber.replace("#url#", url)
                value = value.replace("#username#", username)
                value = value.replace("#password#", password)
                println("Binding variable " + variable + " with " + value)
                sh """
                    ${env.WORKSPACE}/scripts/replace.sh "${env.WORKSPACE}/${env.tempModulePath}" "${variable}" "${value}"
                """                              
            } 
        }
    }
}

return this