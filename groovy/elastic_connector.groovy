
def execute() {
    def host = deployConfig['elastic']['host']
    def port = deployConfig['elastic']['port']
    def enable = deployConfig['elastic']["enable"]
    if (host) {
        if (enable) {
            def variable = "//<enable.elastic>"
            def value = enable.replace("#host#", host)
            value = value.replace("#port#", port.toString())
            println("Binding variable " + variable + " with " + value)
            sh """
                ${env.WORKSPACE}/scripts/replace.sh "${env.WORKSPACE}/${env.tempModulePath}" "${variable}" "${value}"
            """                              
        } 
    } else {
        def variable = "//<enable.elastic>"
        def value = ""
        sh """
            ${env.WORKSPACE}/scripts/replace.sh "${env.WORKSPACE}/${env.tempModulePath}" "${variable}" "${value}"
        """ 
    }
}

return this