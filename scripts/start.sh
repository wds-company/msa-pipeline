#!/bin/bash
# ------------------------------------------------------------------------
# Copyright 2019 Techberry Company Limited. (http://www.techberry.co.th)
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License
# ------------------------------------------------------------------------

set -e

cd /deployed

# Start the process
./apis -D
status=$?
if [ $status -ne 0 ]; then
    echo "Failed to start [apis] process: $status"
    exit $status
elif
    echo "[apis] is started."
fi

./apis_subscriber -D
status=$?
if [ $status -ne 0 ]; then
    echo "Failed to start [apis_subscriber] process: $status"
    exit $status
elif
    echo "[apis_subscriber] is started."
fi

#if [ -f "/deployed/apis_subscriber" ]; then
#    ./apis_subscriber -D
#    status=$?
#    if [ $status -ne 0 ]; then
#        echo "Failed to start [apis_subscriber] process: $status"
#        exit $status
#    elif
#        echo "[apis_subscriber] is started."
#    fi
#fi

# Naive check runs checks once a minute to see if either of the processes exited.
# This illustrates part of the heavy lifting you need to do if you want to run
# more than one service in a container. The container exits with an error
# if it detects that either of the processes has exited.
# Otherwise it loops forever, waking up every 60 seconds

while sleep 60; do
    ps aux |grep apis |grep -q -v grep
    PROCESS_STATUS1=$?
    if [ -f "/deployed/apis_subscriber" ]; then
        ps aux |grep apis_subscriber |grep -q -v grep
        PROCESS_STATUS2=$?
    fi 
    # If the greps above find anything, they exit with 0 status
    # If they are not both 0, then something is wrong
    if [ $PROCESS_STATUS1 -ne 0 ]; then
        echo "The [apis] process has already exited."
        exit 1
    fi
    if [ -f "/deployed/apis_subscriber" ]; then
        if [ $PROCESS_STATUS2 -ne 0 ]; then
            echo "The [apis_subscriber] process has already exited."
            exit 1
        fi
    fi
done