#!/bin/sh

sourcePath=$1
stringReplaceFrom=$2
stringReplaceTo=$3

#### Main 
echo "Replace script is execute."
echo $sourcePath
echo $stringReplaceFrom
echo $stringReplaceTo
cd $sourcePath
#find . -type f -not -path '*/\.*' -exec sed -i "s#$stringReplaceFrom#$stringReplaceTo#g;s#<next_line>#\r\n\t#g" {} +
find . -type f -not -path '*/\.*' -exec sed -i "s;$stringReplaceFrom;$stringReplaceTo;g" {} +

# check file .env
if [ -f ".env" ]
then
    sed -i "s;$stringReplaceFrom;$stringReplaceTo;g" .env
fi