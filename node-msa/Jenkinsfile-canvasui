#!/usr/bin/env groovy

def deployConfig
def bindingVarArray

def notifyLINE(token, status) {
    //def token = "hgSWq03t4oDr9R2leT9ObJ4ctiegicSkkYrxU8dWXF5" // ake
    //def token = "uNt7yepHIi55Xx7TUeMYKnhhI8p7eMGy9mdWmMGKDBG" // mon
    def jobName = env.JOB_NAME +' '+env.BRANCH_NAME
    def buildNo = env.BUILD_NUMBER
      
    def url = 'https://notify-api.line.me/api/notify'
    def message = "${jobName} Build #${buildNo} ${status} \r\n"
    sh "curl ${url} -H 'Authorization: Bearer ${token}' -F 'message=${message}'"
}

pipeline {
    agent any
    options {
        // using the Timestamper plugin we can add timestamps to the console log
        timestamps()
        // And we'd really like to be sure that this build doesn't hang forever, so
        // let's time it out after an hour.
        timeout(time: 30, unit: 'MINUTES')
    }
    parameters {
        string(name: 'MODULE_CONFIG', defaultValue: 'msa-canvasui', description: 'Module Configuration Ex. training-msa-go-tong')
        string(name: 'pushRegistry', defaultValue: 'false', description: 'Enable push registry step')
        string(name: 'deployMsa', defaultValue: 'true', description: 'Enable deployment step')
        string(name: 'gittag', defaultValue: 'develop', description: 'Check out form git tag')
    }
    stages {
        stage('Load Configuration') {
            steps {
                script {
                    def moduleConfig = "${params['MODULE_CONFIG']}"
                    def module = load "${env.WORKSPACE}/groovy/load_configuration.groovy"
                    deployConfig = module.execute(moduleConfig)
                    bindingVarArray = deployConfig['binding_var']
                }
            }
        }
        stage('Create Directory') {
            steps {
                echo "Home workspace path : ${env.WORKSPACE}"
                sh "rm -rf ${env.WORKSPACE}/frontend"                  
                sh "mkdir -p ${env.WORKSPACE}/frontend"                       
            }
        }
        // stage('Get Source') { // Get code
        //     steps {
        //         sh "git clone ssh://git@bitbucket.beebuddy.net:7999/ssquare-apig/canvasui.git ${env.WORKSPACE}/frontend"
        //     }
        // }

        stage('Git Clone Module') {
            parallel {
                stage('Branch')
                {
                    when { expression { params.pushRegistry == 'false' } }
                    steps {
                        script {
                            sh "git clone --branch ${params['gittag']} ssh://git@bitbucket.beebuddy.net:7999/ssquare-apig/canvasui.git ${env.WORKSPACE}/frontend"
                        }
                    }
                }

                stage('Tag')
                {
                    when { expression { params.pushRegistry == 'true' } }
                    steps {
                        script {
                            sh "git clone --branch ${params['gittag']} ssh://git@bitbucket.beebuddy.net:7999/ssquare-apig/canvasui.git ${env.WORKSPACE}/frontend"
                        }
                    }
                }
            }
        }

        stage('Binding Variable') {
            steps {
                script {
                    def module = load "${env.WORKSPACE}/groovy/binding_variable.groovy"
                    module.execute(bindingVarArray)
                }
            }
        }
        stage('Stop Existing Container') {
            steps {
                sh "docker ps -f name=${containerName} -q | xargs --no-run-if-empty docker container stop"
            }
        }
        stage('Remove Existing Container') {
            steps {
                sh "docker container ls -a -fname=${containerName} -q | xargs -r docker container rm"
            }
        }
        stage('Build Remove Existing Image') {
            steps {
                sh """
                    docker image ls \
                        | awk '{ print \$1,\$2,\$3 }' \
                        | grep ${imageName} \
                        | awk '{print \$3 }' \
                        | xargs -I {} docker image rm {}
                    """
            }
        }

        stage('Build Docker Image') {
            steps {
                sh "docker build -f ${dockerInputFile} -t ${imageName} ."
            }
        }

        stage('Deploy Container') {
            steps {
                sh "export DOCKER_HOST=unix:///var/run/docker.sock"
                sh "docker-compose -f ./composes/node/${dockerComposeFile} up -d"
                sh "docker restart nginx_msa"
            }
        }

        stage('Push Registry') {
            when { expression { params.pushRegistry == 'true' } }
            steps {
                sh 'echo "Fi\'iy[0eoe" | docker login --username imgadmin --password-stdin wdsregistry1.beebuddy.net:9900'
                sh "docker tag ${imageName} wdsregistry1.beebuddy.net:9900/${imageName}:${params['gittag']}"
                sh "docker push wdsregistry1.beebuddy.net:9900/${imageName}:${params['gittag']}"
                sh "docker rmi wdsregistry1.beebuddy.net:9900/${imageName}:${params['gittag']}"
            }
        }
    }

    //post {
    //    always {
    //        FREE_MEM = sh (script: 'free -m', returnStdout: true).trim()
    //        echo "Free memory: ${FREE_MEM}"
    //    }
    //}
    post{
        success{
            notifyLINE("5e7r3cUVHcZsL7WIZfCrkvTGQYRlhz9wIvg1hkipdIV", "Canvas UI succeed")
        }
        failure{
            notifyLINE("5e7r3cUVHcZsL7WIZfCrkvTGQYRlhz9wIvg1hkipdIV", "Canvas UI failed")
        }
    }
}
