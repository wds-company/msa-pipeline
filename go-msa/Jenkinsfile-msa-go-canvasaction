#!/usr/bin/env groovy

def deployConfig
def mqttConnectors
def dbConnectors
def bindingVarArray
def moduleArray
def notifyLINE(status) {
    //def token = "oD4q0zpwSO7ECmwosdwKAo2u9eEw6Es5aLPfBh7JgXq"
    def token = "X31EO8mxp77YiA8TZSMpdrJXJV7Mx3o8OswyatUZm4l"
    def jobName = env.JOB_NAME + ' ' + env.BRANCH_NAME
    def buildNo = env.BUILD_NUMBER
      
    def url = 'https://notify-api.line.me/api/notify'
    def message = "${jobName} Build #${buildNo} ${status} \r\n"
    sh "curl ${url} -H 'Authorization: Bearer ${token}' -F 'message=${message}'"
}


pipeline {
    agent any
    options {
        // using the Timestamper plugin we can add timestamps to the console log
        timestamps()
        // Set timeout for build pipeline job.
        timeout(time: 20, unit: 'MINUTES')
    } 
    parameters {
        string(name: 'MODULE_CONFIG', defaultValue: 'msa-canvasaction-api', description: 'Module Configuration Ex. training-msa-go-oat')
    }
    stages {
        stage('Load Configuration') {
            steps {
                script {
                    def moduleConfig = "${params['MODULE_CONFIG']}"
                    def module = load "${env.WORKSPACE}/groovy/load_configuration.groovy"
                    deployConfig = module.execute(moduleConfig)
                    dbConnectors = deployConfig["db_connectors"]
                    mqttConnectors = deployConfig["mqtt_connectors"]
                    bindingVarArray = deployConfig['binding_var']
                    moduleArray = deployConfig['modules'] 
                }
            }
        }
        stage('Prepare Workspace Directory') {
            steps {
                echo "Home workspace path : ${env.WORKSPACE}"
                sh "rm -rf ${env.WORKSPACE}/${moduleDirectory}"
            }
        }
        stage('Copy External Libraries Package') {
            steps {
                sh "cp -Rp /var/jenkins_home/packages ${env.WORKSPACE}"
            }
        }
        stage('Git Clone Template') {
            steps {
                sh "mkdir -p ${env.WORKSPACE}/${env.tempModulePath}"
                sh "git clone ssh://git@bitbucket.partsoto.net:7799/${env.templateRepo} ${env.WORKSPACE}/${env.tempModulePath}"
            }
        }
        stage('Git Clone Common Library') {
            steps {                 
                sh "mkdir -p ${env.WORKSPACE}/${env.commonPath}"
                sh "git clone ssh://git@bitbucket.partsoto.net:7799/${env.commonRepo} ${env.WORKSPACE}/${env.commonPath}"
            }
        }
        stage('Git Clone Module') {
            steps {
                script {
                    def module = load "${env.WORKSPACE}/groovy/git_clone_module.groovy"
                    module.execute(moduleArray)
                }
            }
        }
        stage('Git Clone Sandbox Common') { 
            steps {
                script {
                    def module = load "${env.WORKSPACE}/groovy/git_clone_sandbox_common.groovy"
                    module.execute()
                }
            }
        }
        stage('Binding Variable') {
            steps {
                script {
                    def module = load "${env.WORKSPACE}/groovy/binding_variable.groovy"
                    module.execute(bindingVarArray)
                }
            }
        }
        stage('Init/Enable Redis Connector') {
            steps {
                script {
                    def module = load "${env.WORKSPACE}/groovy/redis_connector.groovy"
                    module.execute()
                }
            }
        }
        stage('Init/Enable Beego Cache') {
            steps {
                script {
                    def module = load "${env.WORKSPACE}/groovy/beego_cache.groovy"
                    module.execute()
                }
            }
        }
        // stage('Init/Enable MQTT Connector') {
        //     steps {
        //         script {
        //             def module = load "${env.WORKSPACE}/groovy/mqtt_connector.groovy"
        //             module.execute(mqttConnectors)
        //         }
        //     }
        // }
        stage('Set Configuration') {
            steps {
                script {
                    def module = load "${env.WORKSPACE}/groovy/set_configuration.groovy"
                    module.execute()
                }
            }
        }
        stage('Stop Container') {
            steps {
                sh "docker ps -f name=${containerName} -q | xargs --no-run-if-empty docker container stop"
            }
        }
        stage('Remove Container') {
            steps {
                sh "docker container ls -a -fname=${containerName} -q | xargs -r docker container rm"
            }
        }
        stage('Remove Image') {
            steps {
                sh """
                    docker image ls \
                        | awk '{ print \$1,\$2,\$3 }' \
                        | grep ${imageName} \
                        | awk '{print \$3 }' \
                        | xargs -I {} docker image rm {}
                    """
            }
        }
        stage('Build Image') {
            steps {
                sh "docker build --build-arg app_user=${env.appUser} --build-arg app_key=${env.appKey} -f ${dockerInputFile} -t ${imageName} ."
            }
        }
        stage('Deploy Container') {
            steps {
                sh "export DOCKER_HOST=unix:///var/run/docker.sock"
                sh "docker-compose -f ./composes/go/${dockerComposeFile} up -d"
                sh "docker restart nginx_msa"
            }
        }      
    }

    post {
        success{
            notifyLINE("succeed")
        }
        failure{
            notifyLINE("failed")
        }
    }
}